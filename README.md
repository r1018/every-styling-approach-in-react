<img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/react/react-original-wordmark.svg" alt="react" width="40" height="40"/>
<img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/css3/css3-original.svg" width="40" height="40" /> 
<img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/sass/sass-original.svg" width="40" height="40"/>
<img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/bootstrap/bootstrap-original.svg" height="40" width="40" /> <br />
<img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/tailwindcss/tailwindcss-original-wordmark.svg" width="200" height="80" />

### Comparing every styling approach in React

- Inline styling
- External css stylesheet (global)
- Scss/Sass external stylesheet (global)
- CSS Modules external stylehseet (local)
- CSS Modules with sass external stylehseet (local)
- Styled Comonents - same component or external file (local)
- Css Frameworks - Bootstrap & Tailwind
