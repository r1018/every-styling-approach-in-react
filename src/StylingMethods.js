import React from "react";
import CssFrameworks from "./CssFrameworks";
import LoginButton from "./LoginButton";
import PaymentButton from "./PaymentButton";
import SubmitButton from "./SubmitButton";

function StylingMethods() {
  return (
    // inline styling
    <div style={{ width: "800px", margin: "0 auto", marginTop: "30px" }}>
      {/* compares css styling methods */}
      <LoginButton />
      <div></div>
      {/* shows how global css files can clash and how it can be rectified with local styling approach */}
      <PaymentButton />
      <div></div>
      {/* uses styled components */}
      <SubmitButton />
      <div></div>
      {/* using css frameworks - bootstrap and tailwind  */}
      <CssFrameworks />
    </div>
  );
}

export default StylingMethods;
