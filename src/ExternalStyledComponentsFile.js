import styled from "styled-components";

export const StyledButtonExternal = styled.button`
  width: 100px;
  height: 50px;
  background-color: green;
  color: white;
  border-radius: 10px;
`;
