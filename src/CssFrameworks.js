import React from "react";
import Button from "react-bootstrap/Button"; // a lot less heavier than the option below
// import { Button, Card, Nav } from "react-bootstrap"; // imports entire library
import "bootstrap/dist/css/bootstrap.min.css";

function CssFrameworks() {
  return (
    <>
      <Button>Bootstrap</Button>
      <br />
      <h1 className="text-7xl font-bold text-red-500">Tailwind</h1>
    </>
  );
}

export default CssFrameworks;
