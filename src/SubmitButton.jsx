import React from "react";
import styled from "styled-components";
import { StyledButtonExternal } from "./ExternalStyledComponentsFile";

const StyledButton = styled.button`
  width: 100px;
  height: 50px;
  background-color: green;
  color: white;
  border-radius: 10px;
`;

function SubmitButton() {
  return <StyledButtonExternal>Submit</StyledButtonExternal>;
}

export default SubmitButton;
