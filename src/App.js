import StylingMethods from "./StylingMethods";

function App() {
  return (
    <div className="App">
      <StylingMethods />
    </div>
  );
}

export default App;
