import React from "react";
import styles from "./PaymentButton.module.css"; // (4). Doesnt clash with the LoginButton.module.css styles

function PaymentButton() {
  // (2). Inherited the same styles from LoginButton.css which we dont want as styles should be scoped locally in each component, not globally unless using css resets.
  return (
    <button
      className={styles.button}
      // className="button"
    >
      Pay Now
    </button>
  );
}

export default PaymentButton;
