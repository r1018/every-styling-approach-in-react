import React from "react";
// import "./LoginButton.css"; // (2). (Applied globally)
// import "./LoginButton.scss"; // (3). (Applied globally)
import styles from "./LoginButton.module.scss"; // (4). (Applied locally)

// 1. Inline Styles - create an object for the style and within that we need a js object - {{}} and then style via camel casing.
// Advantages - can do quick styling and conditional styling inline.
// Disavantages is any inline styles should generally be avoided. No strings needed for values.

// 2. External Stylesheet - In payment button we can see that applying the same button class will inherit the same styles - which works but might not be the best approach. We would have to use BEM to solve applying variations of a class by using - -btn etc.
// Advantages - Avoids using camel case and css is neatly stored in a seperate place. No inline styling.
// Disadvantages - Stylesheet is global. Normal css stylesheets in react aren't scoped to each component so all the styles are global and styles could clash with each other if class names are the same in different components.

// 3. Sass/Scss - create-react-app is already setup to work with sass/scss. All we need to do is npm i node-sass
// Advantages - can use nested sudo elements like hover really nicely. Css on steroids!
// Disadvantages - Its still globally scoped but scss with BEM convention is popular in react to avoid overwriting/styles clashing - makes sure all your classes are unique across components. className="btn-payment" etc. Can become painful in large projects however as the classes can get out of hand.

// 4. CSS Modules - Styles are local/exclusive to the component - cant leak outside/scoped to the component its used in.
// Advantages - Comes out the box in React and can scope locally! Creates unique class so they are all unique.
// Appends arbitrary numbers to the end of the class to keep it unique
// Css modules also works with sass out of the box!

// 5. CSS in JS -
// Advantages - fantastic for applying dynamically styled components by passing in props and using js to style depending on the props

function LoginButton() {
  return (
    // (1).
    <button
      className={styles.button}
      // className="button"

      // style={{
      //   width: "100px",
      //   height: "50px",
      //   backgroundColor: "blue",
      //   color: "white",
      //   border: "none",
      //   borderRadius: "10px",
      // }}
      // style={inlineObjectStyle}
    >
      Login
    </button>
  );
}

export default LoginButton;

// (1). Alternate inline
const inlineObjectStyle = {
  width: "100px",
  height: "50px",
  backgroundColor: "blue",
  color: "white",
  border: "none",
  borderRadius: "10px",
};
